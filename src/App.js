import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MenuSuperior from './pages/MenuSuperior/index';
import Resumo from './pages/Resumo/index';
import Consultas from './pages/Consultas/index';
import Faturamento from './pages/Faturamento/index';
import Footer from './pages/Footer/index';


function App() {
  return (
    <div> 
    <MenuSuperior/>
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <Switch>
            <Route path="/" exact component={Resumo} />                              
            <Route path="/consultas" component={Consultas} />
            <Route path="/faturamento" component={Faturamento} />
          </Switch>
        </div>
      </div>
    </div>
    <Footer />
  </div>
  );
}

export default App;
